const path = require('path');
const express = require('express');
const morgan = require('morgan');
const menuRoutes = require('./routes/menuRoutes');
const orderRoutes = require('./routes/orderRoutes');
const userRoutes = require('./routes/userRoutes');
const AppError = require('./utils/appError');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const globalErrorHandler = require('./controllers/errorController');
const viewRoutes = require('./routes/viewRoutes');
// const menuController = require('./controllers/menuController');

const app = express();

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

//serving static files
app.use(express.static(path.join(process.cwd(), '/frontie/build')));

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

app.use(express.json());

app.use(cors({ origin: 'httpl://localhost:3000', credentials: true }));
app.use(cookieParser());

//routes
app.get('/', (req, res, next) => {
  res.sendFile(path.join(process.cwd(), 'build/index.html'));
});

app.use('/', viewRoutes);
app.use('/api/v1/menu/', menuRoutes);
app.use('/api/v1/order/', orderRoutes);
app.use('/api/v1/user/', userRoutes);
// app.use('api/v1/orderbydate/', router1);

app.all('*', (req, res, next) => {
  // const err = new Error(`Can't find ${req.originalUrl} on this server`);
  // err.status = 'fail';
  // err.statusCode = 404;

  next(new AppError(`Can't find ${req.originalUrl} on this server`, 404));
});

app.use(globalErrorHandler);
module.exports = app;
