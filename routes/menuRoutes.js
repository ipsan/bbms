const express = require('express');
const menuController = require('../controllers/menuController');

const router = express.Router();

router.route('/').get(menuController.getAllItems).post(menuController.addItem);

router
  .route('/:id')
  .get(menuController.getItem)
  .patch(menuController.updateItem)
  .delete(menuController.deleteItem);

module.exports = router;
