const express = require('express');
const orderController = require('../controllers/orderController');
const menuController = require('../controllers/menuController');
const authController = require('../controllers/authController');
const userController = require('../controllers/userController');

const router = express.Router();

router.get('/current-orders', orderController.getActiveOrders);
router.get('/orders/:id', orderController.getOrderById);
router.post('/orders/', orderController.createOrder);
router.get('/menu/', menuController.getAllItems);
router.post('/menu/', menuController.addItem);
router.get('/menu/:id', menuController.getItem);
router.patch('/menu/:id', menuController.updateItem);
router.delete('/menu/delete/:id', menuController.deleteItem);
router.patch('/orders/:id', orderController.updateOrder);
router.patch('/credit/:id', orderController.addCredit);
router.post('/login/', authController.login);
router.post('/register/', authController.signup);
router.get('/logout/', authController.tryLogout);
router.delete('/cancelOrder/:id', orderController.deleteOrder);
router.get('/user/:token', authController.getId);
router.get('/user/get-info/:id', userController.getUser);
router.patch('/user/edit-info/:id', userController.updateUser);
// router.get('/order/orderCreator', viewsController.showOrderCreator);
// router.get('/order/:slug', viewsController.getOrder);

module.exports = router;
