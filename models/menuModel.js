const mongoose = require('mongoose');

const menuSchema = new mongoose.Schema({
  item: {
    type: String,
    required: [true, 'an item must have a name'],
  },
  price: {
    type: Number,
    required: [true, 'an item must have a price'],
  },
});

const Menu = mongoose.model('Menu', menuSchema);

module.exports = Menu;
