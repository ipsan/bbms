const mongoose = require('mongoose');
const slugify = require('slugify');

// const Menu = require('./menuModel');

const orderSchema = new mongoose.Schema({
  tableNumber: {
    type: String,
    required: [true, 'An order must come from a table'],
  },
  timed: {
    type: Date,
    default: Date.now(),
  },
  items: [
    {
      type: mongoose.Schema.ObjectId,
      ref: 'Menu',
      required: [true, 'An order must have items'],
    },
  ],
  total: {
    type: Number,
  },
  active: {
    type: Boolean,
    default: true,
  },
  paid: {
    type: Boolean,
    default: false,
  },
  slug: String,
  credit: {
    type: String,
    default: 'no',
  },
});

orderSchema.pre(/^find/, function (next) {
  this.populate({
    path: 'items',
  });

  next();
});

// orderSchema.pre('save', function (next) {
//   let sum = 0;
//   this.total = this.items.map(async (el) => {
//     const { price } = await Menu.findById(el);
//     sum += price;
//     console.log('sum is: ', sum);
//     return sum;
//   });
//   console.log('total is: ', this.total);
//   next();
// });

orderSchema.pre('save', function (next) {
  this.slug = slugify(
    `hey yo ${this.timed.toLocaleString('en-us', {
      month: 'long',
      year: 'numeric',
      day: 'numeric',
    })}`,
    {
      lower: true,
    }
  );
  next();
});
// orderSchema.post('save', (doc, next) => {
//   let sum = 0;
//   doc.items.forEach(async (el) => {
//     const { price } = await Menu.findById(el);
//     sum += price;
//     console.log('sum is: ', sum);
//     doc.total = sum;
//   });
//   console.log('total is: ', doc.total);
//   next();
// });

const Order = mongoose.model('Order', orderSchema);

module.exports = Order;
