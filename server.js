const mongoose = require('mongoose');
const dotenv = require('dotenv');
const express = require('express');

process.on('uncaughtException', (err) => {
  console.log('Uncaught Exception!! 🦾 Shutting down...');
  console.log(err.name, err.message);
  process.exit(1);
});

dotenv.config({ path: './config.env' });
const app = require('./app');

mongoose
  .connect(process.env.MONGODB_URI || process.env.DATABASE_LOCAL, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log(`DB connection successful`);
  })
  .catch((e) => console.log('DB connection error: ', e));

const port = process.env.PORT || 8080;

const server = app.listen(port, () => {
  console.log(`Connected to port ${port} in ${process.env.NODE_ENV} mode ....`);
});

process.on('unhandledRejection', (err) => {
  console.log('Unhandled Rejection!! Shutting Down... ');
  console.log(err.name, err.message);
  server.close(() => process.exit(1));
});

if (process.env.NODE_ENV === 'production') {
  console.log('hey');
  app.use(express.static('frontie/build'));
}
