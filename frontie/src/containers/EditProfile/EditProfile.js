import { Component } from 'react';
import axios from 'axios';
import Cookies from 'universal-cookie';

const cookie = new Cookies();

class EditProfile extends Component {
  state = {
    name: '',
    email: '',
    password: '',
  };

  changeName = (e) => {
    const name = e;
    this.setState({ name: name });
  };

  changeEmail = (e) => {
    const email = e;
    this.setState({ email: email });
  };

  changePassword = (e) => {
    const password = e;
    this.setState({ password: password });
  };

  updateUser = () => {
    const token = cookie.get('token');
    const { email, name } = this.state;
    const updateItems = { email, name };
    axios.get('/user/' + token).then((res) => {
      axios
        .patch('/user/edit-info/' + res.data.id, updateItems)
        .then((res) => console.log('ok, editing complete: ', res));
    });
  };

  componentDidMount() {
    let { name, email } = this.state;
    const token = cookie.get('token');
    axios.get('/user/' + token).then((res) => {
      console.log('the userID is: ', res.data.id);
      axios.get('/user/get-info/' + res.data.id).then((res) => {
        name = res.data.user.name;
        email = res.data.user.email;
        this.setState({ name: name, email: email });
      });
    });
  }

  render() {
    return (
      <div className="form-group">
        <label>Name</label>
        <input
          type="text"
          name="name"
          onChange={(e) => this.changeName(e.target.value)}
          className="form-control"
          placeholder={this.state.name}
        ></input>
        <label>Email: </label>
        <input
          type="text"
          name="email"
          onChange={(e) => this.changeEmail(e.target.value)}
          className="form-control"
          placeholder={this.state.email}
        ></input>
        <br />

        <button onClick={() => this.updateUser()} className="btn btn-primary">
          Update
        </button>
      </div>
    );
  }
}

export default EditProfile;
