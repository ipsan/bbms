import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import axios from 'axios';

import CurrentOrder from '../../components/CurrentOrder/CurrentOrder';

// import FullOrder from '../../components/FullOrder/FullOrder';

class CurrentOrders extends Component {
  state = {
    orders: [],
    selectedOrderId: null,
  };

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('from CurrentOrderes: ', nextState);
  //   return true;
  // }

  orderSelectedHandler = (id) => {
    this.props.history.push({
      pathname: `/full-order/${id}`,
      state: {
        data: id,
      },
    });
  };

  componentDidMount() {
    axios
      .get('/current-orders')
      .then((res) => {
        console.log('res ma: ', res);
        this.setState({ orders: res.data.orders });
      })
      .catch((err) => console.log('error is: ', err));
  }

  render() {
    const orders = this.state.orders.map((order) => {
      return (
        <CurrentOrder
          tableNumber={order.tableNumber}
          key={order._id}
          active="active"
          clicked={() => this.orderSelectedHandler(order._id)}
        />
      );
    });

    return <div>{orders}</div>;
  }
}
export default CurrentOrders;
