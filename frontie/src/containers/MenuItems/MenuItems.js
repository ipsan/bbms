import { React, Component } from 'react';
import axios from 'axios';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import AllItems from '../../components/Menu/AllItems';
import { AddItem } from '../../components/Menu/AddItem';
import EditItem from '../../components/Menu/EditItem';

class MenuItems extends Component {
  state = {
    menu: [],
    id: '',
    item: '',
    price: null,
    showEditor: false,
    showAdder: false,
  };

  changeItemHandler = (e) => {
    const itemName = e;
    this.setState({ item: itemName });
  };

  editItemHandler = (id) => {
    axios.get('/menu/' + id).then((res) => {
      this.setState({
        id: res.data.item._id,
        item: res.data.item.item,
        price: res.data.item.price,
      });
    });
    this.setState({ showEditor: true });
  };

  changePriceHandler = (e) => {
    const price = e;
    this.setState({ price: price });
  };

  removeItemHandler = (id) => {
    let { menu } = this.state;

    for (var i = 0; i < menu.length; i++) {
      if (menu[i]._id === id) {
        menu.splice(i, 1);
      }
    }

    this.setState({ menu: menu });

    axios.delete('/menu/delete/' + id).then((res) => {
      toast.error('Deleted', {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    });
  };

  addItemHandler = (item, price) => {
    const newItem = { item, price };
    let { menu } = this.state;
    axios.post('/menu', newItem).then((res) => {
      console.log('new item added: ', res.data);
      menu.push(res.data.data.item);
      console.log(res.data.data.item);

      this.setState({ menu: menu });
      toast.success('Item Added', {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    });
  };

  itemUpdateHandler = (itemId, newItem) => {
    let { menu } = this.state;
    let menuArr = Object.values(menu);
    menuArr.forEach((el) => {
      if (el._id === itemId) {
        el.item = newItem.item;
        el.price = newItem.price;
      }
    });

    axios.patch('/menu/' + itemId, newItem).then((res) => {
      this.setState({ showEditor: false, menu: menuArr });
      toast.info('Update Successful', {
        position: 'top-right',
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    });
  };

  closeModalHandler = () => {
    this.setState({ showEditor: false, showAdder: false });
  };

  componentDidMount() {
    axios.get('/menu').then((res) => {
      this.setState({ menu: res.data.menu });
    });
  }

  render() {
    return (
      <div>
        <ToastContainer />

        {this.state.showAdder ? (
          <div onClick={this.closeModalHandler} className="back-drop"></div>
        ) : null}

        {this.state.showEditor ? (
          <div onClick={this.closeModalHandler} className="back-drop"></div>
        ) : null}
        <button
          onClick={() => this.setState({ showAdder: true })}
          className="btn-openModal"
        >
          Add Item
        </button>
        <AddItem
          show={this.state.showAdder}
          close={this.closeModalHandler}
          itemAdder={this.addItemHandler}
        />
        <EditItem
          show={this.state.showEditor}
          close={this.closeModalHandler}
          itemUpdater={this.itemUpdateHandler}
          _id={this.state.id}
          item={this.state.item}
          price={this.state.price}
        />
        <AllItems
          items={this.state.menu}
          removeItem={this.removeItemHandler}
          editItem={this.editItemHandler}
        />
      </div>
    );
  }
}

export default MenuItems;
