import { Component } from 'react';
import Cookie from 'universal-cookie';
import axios from 'axios';

const cookie = new Cookie();

class Login extends Component {
  state = {
    email: '',
    password: '',
  };

  loginTry = () => {
    console.log('tries logging in 🥂');
    const { email, password } = this.state;
    console.log(`email is: ${email} and password is: ${password}`);
    const loginCreds = { email, password };

    axios.post('/login/', loginCreds, { withCredentials: true }).then((res) => {
      console.log('okay', res.data.data.user.name);
      console.log('cookie is: ', cookie.get('token'));
      console.log('type is: ', typeof cookie.get('token'));
      console.log('this props is: ', this.props);
      console.log('You in MF');
      localStorage.setItem('name', res.data.data.user.name);
      this.props.history.push({
        pathname: '/current-orders',
      });
    });
  };

  changeEmail = (e) => {
    const email = e;
    this.setState({ email: email });
  };

  changePassword = (e) => {
    const password = e;
    this.setState({ password: password });
  };

  componentDidMount() {
    console.log('it is:');
  }

  render() {
    return (
      <div className="form-group">
        <label>Email: </label>
        <input
          type="text"
          name="username"
          onChange={(e) => this.changeEmail(e.target.value)}
          className="form-control"
        ></input>
        <label>Password: </label>
        <input
          type="password"
          name="password"
          onChange={(e) => this.changePassword(e.target.value)}
          className="form-control"
        ></input>
        <br />
        <br />
        <button onClick={() => this.loginTry()} className="btn btn-primary">
          LOGin
        </button>
      </div>
    );
  }
}

export default Login;
