import React, { Component } from 'react';
import axios from 'axios';
// import { Link } from 'react-router-dom';
import classes from './NewOrder.module.css';

class NewOrder extends Component {
  state = {
    tableNumber: '',
    items: undefined,
    menu: [],
    itemsList: [],
    total: 0,
  };

  componentDidMount() {
    axios.get('/menu/').then((res) => {
      this.setState({ menu: res.data.menu });
    });
  }
  // shouldComponentUpdate(nextProps, nextState) {
  //   // console.log('nextPorps: ', nextProps);
  //   console.log('nextState: ', nextState);
  //   return true;
  // }
  orderDataHandler = () => {
    const order = {
      tableNumber: this.state.tableNumber,
      items: this.state.itemsList.map((el) => el._id),
    };
    console.log('order is: ', order);

    axios
      .post('/orders/', order)
      .then((response) => {
        alert('New Order Created');
        this.setState({ tableNumber: '' });
        this.setState({ itemsList: [] });
      })
      .catch((err) => console.log(err));
  };

  itemOnUpdateHandler = (item) => {
    const tempItem = [...this.state.itemsList];
    let tempTotal = 0;
    axios.get('/menu/' + item).then((res) => {
      tempItem.push(res.data.item);
      this.setState({ itemsList: tempItem });
      tempTotal = tempItem.reduce((acc, el) => {
        return acc + el.price;
      }, 0);
      this.setState({ total: tempTotal });
    });
  };

  itemRemovalHandler = (id) => {
    const tempItems = [...this.state.itemsList];
    let { total } = { ...this.state };

    for (var i = 0; i < tempItems.length; i++) {
      if (tempItems[i]._id === id) {
        total = total - tempItems[i].price;
        tempItems.splice(i, 1);
      }
    }

    this.setState({ itemsList: tempItems }, () => {
      console.log('current itemslist in state: ', this.state.itemsList);
    });
    this.setState({ total: total });
  };

  // tryHandler = () => {
  //   console.log(this.props.history);
  //   this.props.history.push({
  //     pathname: '/about/test',
  //     state: {
  //       data: ['apple', 'not apple'],
  //     },
  //   });
  // };

  render() {
    let menu = (
      <select>
        <option>loading...</option>
      </select>
    );

    if (this.state.menu) {
      menu = (
        <select
          onChange={(e) => {
            this.itemOnUpdateHandler(e.target.value);
            e.target.value = 'hehehe';
          }}
        >
          <option value="hehehe" hidden>
            ...
          </option>
          {this.state.menu.map((item) => (
            <option key={item._id} value={item._id}>
              {item.item}
            </option>
          ))}
          ;
        </select>
      );
    }

    let description = <p>Ze GaatDamn items should appear</p>;
    if (this.state.itemsList.length > 0) {
      description = this.state.itemsList.map((el) => {
        return (
          <p
            key={el._id + Math.random()}
            onClick={() => this.itemRemovalHandler(el._id)}
          >
            {el.item}: Rs.{el.price}
          </p>
        );
      });
    }

    let total = <p>Rs.0</p>;
    if (this.state.total) {
      total = <p>Rs.{this.state.total}</p>;
    }

    return (
      <div className={classes.NewOrder}>
        <h1>Add an Order</h1>
        <label>Table Number</label>
        <input
          type="text"
          value={this.state.tableNumber}
          onChange={(event) =>
            this.setState({ tableNumber: event.target.value })
          }
        />
        <label>Items</label>
        {description}
        <label>Total</label>
        {total}
        <label>Item</label>
        {menu}
        <button onClick={this.orderDataHandler}>Add Order</button>
      </div>
    );
  }
}

export default NewOrder;
