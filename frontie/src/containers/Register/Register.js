import { Component } from 'react';
import axios from 'axios';

class Register extends Component {
  state = {
    name: '',
    email: '',
    password: '',
  };

  changeName = (e) => {
    const name = e;
    this.setState({ name: name });
  };

  changeEmail = (e) => {
    const email = e;
    this.setState({ email: email });
  };

  changePassword = (e) => {
    const password = e;
    this.setState({ password: password });
  };

  registerTry = () => {
    const { name, email, password } = this.state;
    const registerCreds = { name, email, password };

    axios
      .post('/register/', registerCreds, () => {
        console.log('regged');
      })
      .then((res) => {
        console.log(res.data);
      })
      .catch((e) => console.log('error: ', e));
  };

  componentDidMount() {
    console.log('wooow');
  }
  render() {
    return (
      <div className="form-group">
        <label>Name</label>
        <input
          type="text"
          name="password"
          onChange={(e) => this.changeName(e.target.value)}
          className="form-control"
        ></input>
        <label>Email: </label>
        <input
          type="text"
          name="username"
          onChange={(e) => this.changeEmail(e.target.value)}
          className="form-control"
        ></input>
        <label>Password: </label>
        <input
          type="text"
          name="password"
          onChange={(e) => this.changePassword(e.target.value)}
          className="form-control"
        ></input>
        <br />
        <br />
        <button onClick={() => this.registerTry()} className="btn btn-primary">
          Register
        </button>
      </div>
    );
  }
}

export default Register;
