import React from 'react';

import classes from './CurrentOrder.module.css';

const currentOrder = (props) => {
  return (
    <article className={classes.CurrentOrder} onClick={props.clicked}>
      <h1>Table: {props.tableNumber}</h1>
      <div className={classes.Info}>
        <div className={classes.Author}>{props.active}</div>
      </div>
    </article>
  );
};

export default currentOrder;
