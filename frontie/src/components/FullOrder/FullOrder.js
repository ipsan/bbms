import React, { Component } from 'react';
import axios from 'axios';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import classes from './FullOrder.module.css';
import { PayLater } from './PayLater/PayLater';
// import { updateOrder } from '../../../../controllers/orderController';

class FullOrder extends Component {
  state = {
    loadedOrder: null,
    makeNull: false,
    oldProps: null,
    menu: [],
    newItemsList: [],
    total: 0,
    showPayLater: false,
  };

  componentDidUpdate() {
    if (this.props !== this.state.oldProps) {
      if (this.props.id) {
        if (
          !this.state.loadedOrder ||
          (this.state.loadedOrder &&
            this.state.loadedOrder._id !== this.props.id)
        ) {
          axios.get('/orders/' + this.props.id).then((res) => {
            this.setState({ loadedOrder: res.data.order, makeNull: false });
          });
        }
      }
    }
  }

  checkOutHandler = (tableNumber) => {
    var checkoutAlert = window.confirm('Checkout?');
    let tempOrder;
    let items;
    let finalOrder = {};
    if (checkoutAlert === true) {
      if (this.state.newItemsList.length < 1) {
        tempOrder = { ...this.state.loadedOrder };
        tempOrder.paid = true;
        tempOrder.active = false;
        console.log('final unchanged order is: ', tempOrder);
        axios
          .patch(`/orders/${this.props.location.state.data}`, tempOrder)
          .then((res) => {
            console.log('Unchanged checkout: ', res);
            this.setState({ loadedOrder: null, oldProps: this.props }, () =>
              console.log('CHECKOUT COMPLETE: assumed the bill was cleared~')
            );
          });
      } else {
        tempOrder = { ...this.state.newItemsList };
        items = Object.values(tempOrder);
        items = items.map((item) => item._id);

        finalOrder = { items, tableNumber };
        finalOrder.paid = true;
        finalOrder.active = false;

        axios
          .patch(`/orders/${this.props.location.state.data}`, finalOrder)
          .then((res) => {
            console.log('Changed checkout: ', res);
            this.setState({ loadedOrder: null, oldProps: this.props }, () =>
              console.log('CHECKOUT COMPLETE: assumed the bill was cleared~')
            );
          });
      }
    } else {
      console.log('Pressed NOT OK');
    }
  };

  itemRemovalHandler = (id) => {
    let tempItems = [...this.state.newItemsList];

    if (this.state.newItemsList.length < 1 && this.state.total !== 0) {
      tempItems = [...this.state.loadedOrder.items];
    }

    let { total } = { ...this.state };
    console.log('from the remover, total is: ', total);
    for (var i = 0; i < tempItems.length; i++) {
      if (tempItems[i]._id === id) {
        total = total - tempItems[i].price;
        tempItems.splice(i, 1);
      }
    }

    this.setState({ newItemsList: tempItems }, () => {
      console.log('current itemslist in state: ', this.state.newItemsList);
      if (tempItems.length < 1) {
        this.setState({ loadedOrder: null });
      }
    });
    this.setState({ total: total });
  };

  itemOnUpdateHandler = (item) => {
    let tempItem = [...this.state.newItemsList];
    if (this.state.newItemsList.length < 1) {
      // console.log('newItemsList is Empty');
      tempItem = [...this.state.loadedOrder.items];
    }
    let tempTotal = this.state.total;
    // console.log('new total is: ', tempTotal);
    axios.get('/menu/' + item).then((res) => {
      tempItem.push(res.data.item);
      this.setState({ newItemsList: tempItem });
      tempTotal = tempItem.reduce((acc, el) => {
        return acc + el.price;
      }, 0);
      this.setState({ total: tempTotal });
    });
  };

  cancelOrderHandler = () => {
    const orderCancelAlert = window.confirm('Checkout?');

    if (orderCancelAlert === true) {
      console.log('loaded order: ', this.state.loadedOrder._id);
      axios.delete('/cancelOrder/' + this.state.loadedOrder._id).then((res) => {
        console.log('maybe cancelled: ', res);
        this.setState({ loadedOrder: null });
      });
    } else {
      console.log('Order not cancelled yet');
    }
  };

  updateOrderHandler = (tableNumber) => {
    var checkoutAlert = window.confirm('Update?');
    let tempOrder;
    let items;
    let finalOrder = {};
    if (checkoutAlert === true) {
      if (this.state.newItemsList.length < 1) {
        tempOrder = { ...this.state.loadedOrder };
        tempOrder.paid = false;
        tempOrder.active = true;
        console.log('unchanged order is: ', tempOrder);
      } else {
        tempOrder = { ...this.state.newItemsList };
        items = Object.values(tempOrder);
        items = items.map((item) => item._id);

        finalOrder = { items, tableNumber };
        finalOrder.paid = false;
        finalOrder.active = true;

        axios
          .patch(`/orders/${this.props.location.state.data}`, finalOrder)
          .then((res) => {
            console.log('Changed update: ', res);
            toast.success(`Order Updated`, {
              position: 'top-right',
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          });
      }
    } else {
      console.log('Pressed NOT OK');
    }
  };

  closeModalHandler = () => {
    this.setState({ showPayLater: false });
  };

  addCreditHandler = (name) => {
    console.log(`Id is ${this.state.loadedOrder._id} and name is: ${name}`);

    axios
      .patch(`/credit/${this.state.loadedOrder._id}`, { name })
      .then((res) => {
        console.log('res is: ', res);
        this.setState({ loadedOrder: null });
        toast.success(`Credit added to ${name}`, {
          position: 'top-right',
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  };

  componentDidMount() {
    console.log('props from fullOrder: ', this.props);
    let { total } = { ...this.state };
    axios.get(`/orders/${this.props.location.state.data}`).then((res) => {
      this.setState({ loadedOrder: res.data.order, makeNull: false });
      this.state.loadedOrder.items.forEach((item) => {
        total = total + item.price;
      });
      console.log('total should be: ', total);
      this.setState({ total: total });
    });
    axios.get('/menu').then((res) => {
      this.setState({ menu: res.data.menu });
    });
  }

  render() {
    let order = (
      <p style={{ textAlign: 'center' }}>
        Please select an Order from the Orders Menu
      </p>
    );

    if (this.state.loadedOrder) {
      let items = [];
      if (this.state.newItemsList.length < 1) {
        items = this.state.loadedOrder.items.map((item) => (
          <div key={item.item + Math.random()}>
            <p onClick={() => this.itemRemovalHandler(item._id)}>
              {item.item}({item.price})
            </p>
          </div>
        ));
      } else {
        console.log('items from new: ', this.state.newItemsList);
        items = this.state.newItemsList.map((item) => (
          <div key={item.item + Math.random()}>
            <p onClick={() => this.itemRemovalHandler(item._id)}>
              {item.item}({item.price})
            </p>
          </div>
        ));
      }

      const { total } =
        // this.state.total === null
        //   ? this.state.loadedOrder.items.reduce((sum, item) => {
        //       sum = sum + item.price;
        //       return sum;
        //     }, 0):
        this.state;

      order = (
        <div className={classes.FullOrder}>
          <h1>Table: {this.state.loadedOrder.tableNumber}</h1>
          {items}
          Total: {total}
          <div className={classes.Edit}>
            <button
              onClick={() =>
                this.checkOutHandler(this.state.loadedOrder.tableNumber)
              }
            >
              Checkout
            </button>
            <button onClick={this.cancelOrderHandler}>Cancel Order</button>
            <button
              onClick={() =>
                this.updateOrderHandler(this.state.loadedOrder.tableNumber)
              }
            >
              Update Order
            </button>
            <button
              onClick={() => this.setState({ showPayLater: true })}
              className="btn-openModal"
            >
              Pay Later?
            </button>
          </div>
        </div>
      );
    }
    return (
      <div>
        <ToastContainer />
        {this.state.showPayLater ? (
          <div onClick={this.closeModalHandler} className="back-drop"></div>
        ) : null}

        <PayLater
          show={this.state.showPayLater}
          close={this.closeModalHandler}
          addCredit={this.addCreditHandler}
        />

        {order}
        <select
          onChange={(e) => {
            this.itemOnUpdateHandler(e.target.value);
            e.target.value = 'hehehe';
          }}
        >
          <option value="hehehe">...</option>
          {this.state.menu.map((item) => (
            <option key={item._id} value={item._id}>
              {item.item}
            </option>
          ))}
          ;
        </select>
      </div>
    );
  }
}

export default FullOrder;
