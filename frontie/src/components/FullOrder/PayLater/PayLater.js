import React, { useState } from 'react';
import './PayLater.css';
import classes from './PayLaterStyle.module.css';

export const PayLater = (props) => {
  const [name, setName] = useState('');

  const clearFields = () => {
    setName('');
  };

  return (
    <div
      className="modal-wrapper"
      style={{
        transform: props.show ? 'translateY(0vh)' : 'translateY(-100vh)',
        opacity: props.show ? '1' : '0',
      }}
    >
      <div className="modal-header">
        <p>Credit?</p>
        <span onClick={props.close} className="close-modal-btn">
          x
        </span>
      </div>
      <div className="modal-content">
        <div className="modal-body">
          <div className="form-group">
            <label>Name:</label>
            <br />
            <input
              type="text"
              className="form-control"
              value={name || ''}
              onChange={(e) => {
                setName(e.target.value);
              }}
            ></input>
            <br />
            <button
              className={classes.buttonGreen}
              onClick={() => {
                props.addCredit(name);
                clearFields();
                props.close();
              }}
            >
              Add Credit
            </button>
            <button
              className={classes.buttonRemove}
              style={{
                marginLeft: '20px',
              }}
              onClick={() => {
                clearFields();
                props.close();
              }}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
