import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
// import classes from './Dropdown.module.css';
import './Header.css';
import Cookies from 'universal-cookie';

const cookie = new Cookies();

const jumpToProfileEdit = (selectedValue, props) => {
  console.log('selected value: ', selectedValue);
  if (selectedValue === 'logout') {
    tryLogouter(props);
  } else {
    editProfileHandler(props);
  }
};

const tryLogouter = (props) => {
  cookie.remove('token');
  props.history.push('/');
};

const editProfileHandler = (props) => {
  console.log('props in the profile: ', props);
  props.history.push({
    pathname: '/profile',
  });
};

function DropdownMenu(props) {
  return (
    <div className="dropdown">
      <button
        className="menu-item"
        onClick={() => {
          editProfileHandler(props.props.routeProps);
          props.openClose(false);
        }}
      >
        Edit User
      </button>
      <button
        className="menu-item"
        onClick={() => tryLogouter(props.props.routeProps)}
      >
        Logout
      </button>
    </div>
  );
}

const Header = (props) => {
  const [open, setOpen] = useState(false);

  console.log('props in header', props);
  let menu = props.isLoggedIn ? (
    <ul className="navbar-nav">
      <li className="nav-item">
        <NavLink className="nav-link" to="/current-orders">
          Current Orders
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/add-new">
          Add New
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/menu">
          Menu
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" to="/search">
          Search
        </NavLink>
      </li>
      {/* <li className="nav-item">
        {' '}
        <button
          onClick={() => tryLogouter(props.routeProps)}
          style={{ border: 'none', background: 'none', padding: 'none' }}
          className="nav-link"
        >
          Logout
        </button>
      </li> */}
      {/* <li>
        <button
          onClick={() => {
            editProfileHandler(props.routeProps);
          }}
          style={{
            border: 'none',
            background: 'none',
            position: 'fixed',
            right: '10px',
          }}
          className="nav-link"
        >
          {localStorage.getItem('name')}
        </button>
      </li> */}

      {/* Profile dropdown */}

      {/* <li>
        <select
          onChange={(e) => {
            jumpToProfileEdit(e.target.value, props.routeProps);
            e.target.value = 'hehehe';
          }}
          style={{
            border: 'none',
            background: 'none',
            position: 'fixed',
            right: '10px',
          }}
          className="nav-link"
        >
          <option value="hehehe" hidden>
            {localStorage.getItem('name')}
          </option>
          <option value="edit">Edit User</option>
          <option value="logout">Logout</option>
        </select>
      </li> */}

      <li className="nav-item">
        <button
          onClick={() => {
            setOpen(!open);
          }}
          className={open ? 'nav-link-drp active' : 'nav-link-drp'}
        >
          {' '}
          {localStorage.getItem('name')}
        </button>
        {open && <DropdownMenu props={props} openClose={setOpen} />}
      </li>
    </ul>
  ) : (
    <ul className="navbar-nav">
      <li className="nav-item">
        {' '}
        <NavLink className="nav-link" exact to="/">
          Home
        </NavLink>
      </li>
      <li className="nav-item">
        {' '}
        <NavLink className="nav-link" to="/register">
          Register
        </NavLink>
      </li>
      <li className="nav-item">
        {' '}
        <NavLink className="nav-link" to="/login">
          Login
        </NavLink>
      </li>
    </ul>
  );

  return (
    <div className="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
      {menu}
    </div>
  );
};

export default Header;
