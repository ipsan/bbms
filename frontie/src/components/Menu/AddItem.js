import React, { useState } from 'react';
import './AddItem.css';
import classes from './AllItems.module.css';

export const AddItem = (props) => {
  const [item, setItem] = useState('');
  const [price, setPrice] = useState(null);

  const clearFields = () => {
    setPrice(null);
    setItem('');
  };

  return (
    <div
      className="modal-wrapper"
      style={{
        transform: props.show ? 'translateY(0vh)' : 'translateY(-100vh)',
        opacity: props.show ? '1' : '0',
      }}
    >
      <div className="modal-header">
        <p>Add Item</p>
        <span onClick={props.close} className="close-modal-btn">
          x
        </span>
      </div>
      <div className="modal-content">
        <div className="modal-body">
          <div className="form-group">
            <label>Item</label>
            <br />

            <input
              type="text"
              className="form-control"
              value={item}
              onChange={(e) => {
                setItem(e.target.value);
              }}
            ></input>
            <br />
            <label>Price</label>
            <br />
            <input
              type="price"
              className="form-control"
              value={price || ''}
              onChange={(e) => {
                setPrice(e.target.value);
              }}
            ></input>
            <br />
            <button
              className={classes.buttonGreen}
              onClick={() => {
                props.itemAdder(item, price);
                clearFields();
                props.close();
              }}
            >
              Add Item
            </button>
            <button
              className={classes.buttonRemove}
              style={{
                marginLeft: '20px',
              }}
              onClick={() => {
                clearFields();
                props.close();
              }}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
