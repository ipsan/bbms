import React, { useEffect, useState } from 'react';

import classes from './AllItems.module.css';

const EditItem = (props) => {
  const [item, setItem] = useState('');
  const [price, setPrice] = useState(null);

  useEffect(() => {
    setItem(props.item);
    setPrice(props.price);
  }, [props.item, props.price]);

  return (
    <div
      className="modal-wrapper"
      style={{
        transform: props.show ? 'translateY(0vh)' : 'translateY(-100vh)',
        opacity: props.show ? '1' : '0',
      }}
    >
      <div className="modal-header">
        <p>Update Item</p>
        <span onClick={props.close} className="close-modal-btn">
          x
        </span>
      </div>
      <div className="modal-content">
        <div className="modal-body">
          <div className="form-group">
            <label>Item</label>
            <br />

            <input
              type="text"
              className="form-control"
              value={item ? item : ''}
              onChange={(e) => {
                setItem(e.target.value);
              }}
            ></input>
            <br />
            <label>Price</label>
            <br />
            <input
              type="Number"
              className="form-control"
              value={price ? price : ''}
              onChange={(e) => {
                setPrice(e.target.value);
              }}
            ></input>
            <br />
            <button
              className={classes.buttonGreen}
              onClick={() => {
                props.itemUpdater(props._id, { item, price });
              }}
            >
              Update Item
            </button>
            <button
              className={classes.buttonRemove}
              style={{
                marginLeft: '20px',
              }}
              onClick={() => {
                props.close();
              }}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditItem;
