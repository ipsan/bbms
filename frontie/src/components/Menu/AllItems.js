import React from 'react';

import classes from './AllItems.module.css';

const AllItems = (props) => {
  const menuData = props.items.map((item) => {
    return (
      <tr key={item._id}>
        <td>{item.item}</td>
        <td>{item.price}</td>

        <td className={classes.tdls}>
          <button
            className={classes.buttonGreen}
            onClick={() => {
              props.editItem(item._id);
            }}
          >
            Edit
          </button>
          <button
            className={classes.buttonRemove}
            onClick={() => {
              props.removeItem(item._id);
            }}
          >
            Remove
          </button>
        </td>
      </tr>
    );
  });
  return (
    <div className={classes.table}>
      <table>
        <thead>
          <tr>
            <th>Item</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{menuData}</tbody>
      </table>
    </div>
  );
};

export default AllItems;
