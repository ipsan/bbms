// import logo from './logo.svg';
import './App.css';
import AppRouting from './Routing';
// import Header from './components/Common/Header/Header';

function App() {
  return (
    <div className="App">
      <AppRouting />
    </div>
  );
}

export default App;
