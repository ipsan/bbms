import React, { Component } from 'react';
import axios from 'axios';

export default class Weather extends Component {
  constructor() {
    super();
    this.state = {
      weather: 'Not yet Available',
    };
  }

  handleButtonClick = () => {
    axios.get('/getWeatherToronto').then((response) => {
      this.setState({ weather: response.data.data.menu[0].item });
      console.log(response.data);
    });
  };
  render() {
    return (
      <div>
        <button onClick={this.handleButtonClick}>Get weather in Toronto</button>
        <h1>The Weather in Toronto is: {this.state.weather}</h1>
      </div>
    );
  }
}
