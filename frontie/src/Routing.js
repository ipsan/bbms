import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import React from 'react';
import CurrentOrders from './containers/CurrentOrders/CurrentOrders';
import NewOrder from './containers/NewOrder/NewOrder';
import Header from './components/Common/Header/Header';
import FullOrder from './components/FullOrder/FullOrder';
import Login from './containers/Login/Login';
import Register from './containers/Register/Register';
import EditProfile from './containers/EditProfile/EditProfile';
import MenuItems from './containers/MenuItems/MenuItems';
import SearchMain from './components/SearchMain/SearchMain';

import Cookies from 'universal-cookie';

const cookie = new Cookies();

const AppRouting = (props) => {
  const About = (props) => {
    console.log('props in about: ', props);
    return <p>About</p>;
  };
  // const Contact = (props) => <p>Contact</p>;
  const Settings = (props) => <p>Settings</p>;
  // const Register = (props) => <p>Register</p>;
  const Home = (props) => <p>Home</p>;

  const NotFound = (props) => (
    <div>
      <p>ERR: 404 NOT FOUND</p>
      <img src="img/notfound.jpeg" alt="imager"></img>
    </div>
  );

  const ProtectedRoute = ({ component: Component, ...rest }) => {
    return (
      <Route
        {...rest}
        render={(routeProps) => {
          return cookie.get('token') ? (
            <>
              <Header isLoggedIn={true} routeProps={routeProps}>
                {' '}
                <p>Hello</p>
              </Header>

              <div className="main">
                <Component {...routeProps}></Component>
              </div>
            </>
          ) : (
            <Redirect to="/login"> </Redirect>
          );
        }}
      ></Route>
    );
  };

  const PublicRoute = ({ component: Component, ...rest }) => {
    return (
      <Route
        {...rest}
        render={(routeProps) => (
          <>
            <Header isLoggedIn={cookie.get('token') ? true : false}></Header>

            <div className="main">
              <Component {...routeProps}></Component>
            </div>
          </>
        )}
      ></Route>
    );
  };

  return (
    <BrowserRouter>
      <Switch>
        <PublicRoute path="/" exact component={Login}></PublicRoute>
        <ProtectedRoute path="/new-order" component={NewOrder}></ProtectedRoute>
        <ProtectedRoute path="/add-new" component={NewOrder}></ProtectedRoute>
        <ProtectedRoute path="/menu" component={MenuItems}></ProtectedRoute>
        <ProtectedRoute
          path="/full-order/:id"
          component={FullOrder}
        ></ProtectedRoute>
        <ProtectedRoute path="/settings" component={Settings}></ProtectedRoute>
        <ProtectedRoute
          path="/profile"
          component={EditProfile}
        ></ProtectedRoute>
        <ProtectedRoute path="/logout" component={Login}></ProtectedRoute>
        <ProtectedRoute path="/search" component={SearchMain}></ProtectedRoute>
        <PublicRoute path="/register" component={Register}></PublicRoute>
        <PublicRoute path="/login" component={Login}></PublicRoute>
        <ProtectedRoute
          path="/current-orders"
          component={CurrentOrders}
        ></ProtectedRoute>
        <PublicRoute path="/about/:parmiter" component={About}></PublicRoute>
        <PublicRoute path="/home" component={Home}></PublicRoute>
        <PublicRoute component={NotFound} />;
      </Switch>
    </BrowserRouter>
  );
};

export default AppRouting;
