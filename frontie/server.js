const express = require('express');
const path = require('path');

const app = express();

//server static files
app.use(express.static(path.join(process.cwd(), 'build')));

//handler
app.get('/*', (req, res, next) => {
  res.sendFile(path.join(process.cwd(), 'build/index.html'));
});

const port = process.env.PORT || 8080;

//listen
app.listen(port, function (err, done) {
  if (!err) {
    console.log(`server listening on port ${port}`);
  }
});
