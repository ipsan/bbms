const Menu = require('../models/menuModel');
const Order = require('../models/orderModel');
const catchAsync = require('../utils/catchAsync');

exports.getOverview = catchAsync(async (req, res) => {
  //1) Get data from collection
  const orders = await Order.find();

  //2) build template

  //3) Render the template using data from 1)

  res.status(200).render('overview', {
    orders,
  });
});

exports.getOrder = catchAsync(async (req, res) => {
  const order = await Order.findOne({ slug: req.params.slug });
  console.log('order items: ', order.items);
  res.status(200).render('order', {
    title: order.tableNumber,
    order,
  });
});

exports.showOrderCreator = catchAsync(async (req, res, next) => {
  const items = await Menu.find();
  const ranString = 'what';
  const itemsArr = [1, 2];
  res.status(200).render('orderCreator', {
    items,
    itemsArr,
    ranString,
  });
});
