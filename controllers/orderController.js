const Order = require('../models/orderModel');
const catchAsync = require('../utils/catchAsync');

module.exports.createOrder = catchAsync(async (req, res, next) => {
  const order = await Order.create(req.body);
  res.status(201).json({
    status: 'success',
    data: {
      order,
    },
  });
});

module.exports.getAllOrders = catchAsync(async (req, res, next) => {
  const orders = await Order.find();
  res.status(200).json({
    status: 'success',
    orders,
  });
});

module.exports.getActiveOrders = catchAsync(async (req, res, next) => {
  const orders = await Order.find({ active: true });
  res.status(200).json({
    status: 'success',
    orders,
  });
});

module.exports.getOrderById = catchAsync(async (req, res, next) => {
  const order = await Order.findById(req.params.id);
  res.status(200).json({
    status: 'success',
    order,
  });
});

module.exports.getOrdersByDate = catchAsync(async (req, res, next) => {
  const orders = await Order.find({
    timed: { $gte: new Date(req.params.date) },
  });
  res.status(200).json({
    status: 'success',
    results: orders.length,
    data: {
      orders,
    },
  });
});

module.exports.updateOrder = catchAsync(async (req, res, next) => {
  const newOrder = await Order.findByIdAndUpdate(
    req.params.id,
    { items: req.body.items, paid: req.body.paid, active: req.body.active },
    {
      new: true,
      runValidators: true,
    }
  );
  let tempValueHolder = 0;

  const totalUntil = newOrder.items.map((el) => {
    tempValueHolder += el.price;
    return tempValueHolder;
  });

  newOrder.total = totalUntil[totalUntil.length - 1];
  await newOrder.save();

  res.status(201).json({
    status: 'success',
    data: newOrder,
  });
});

module.exports.deleteOrder = catchAsync(async (req, res) => {
  const order = await Order.findByIdAndDelete(req.params.id);
  res.status(204).json({
    status: 'success',
    order,
  });
});

module.exports.addCredit = catchAsync(async (req, res) => {
  const creditOrder = await Order.findByIdAndUpdate(
    req.params.id,
    {
      active: false,
      paid: false,
      credit: req.body.name,
    },
    {
      runValidators: true,
      new: true,
    }
  );

  res.status(201).json({
    status: 'success',
    creditOrder,
  });
});
