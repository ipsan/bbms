const User = require('../models/userModel');
const catchAsync = require('../utils/catchAsync');

module.exports.createUser = catchAsync(async (req, res) => {
  const user = await User.create(req.body);
  res.status(201).json({
    status: 'success',
    data: {
      user,
    },
  });
});

module.exports.getAllUsers = catchAsync(async (req, res) => {
  const users = await User.find();
  res.status(200).json({
    status: 'success',
    data: users,
  });
});

module.exports.getUser = catchAsync(async (req, res) => {
  const user = await User.findById(req.params.id);
  res.status(200).json({
    status: 'success',
    user,
  });
});

module.exports.updateUser = catchAsync(async (req, res) => {
  const updatedUser = await User.findByIdAndUpdate(
    req.params.id,
    {
      email: req.body.email,
      name: req.body.name,
    },
    { new: true }
  );
  res.status(202).json({
    status: 'success',
    updatedUser,
  });
});
