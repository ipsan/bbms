const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/appError');

const User = require('../models/userModel');

const signToken = (id) =>
  jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });

const createSendToken = (user, statusCode, res) => {
  const token = signToken(user._id);
  res
    .status(statusCode)
    .cookie('token', token, {
      sameSite: 'strict',
      path: '/',
      expires: new Date(new Date().getTime + 100 * 1000),
    })
    .json({
      status: 'success',
      token,
      data: {
        user,
      },
    });
};

exports.signup = catchAsync(async (req, res, next) => {
  const newUser = await User.create(req.body).catch((e) => console.log(e));
  console.log(newUser);
  createSendToken(newUser, 201, res);
});

exports.login = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;

  //1) check if email and password exist

  if (!email || !password) {
    return next(new AppError('Please provide email and password', 400));
  }

  //2) check if user exists and password is correct
  //below finding user by their email and also selecting password specifically because it has been set to false in the model

  const user = await User.findOne({ email });

  if (!user || user.password !== password) {
    console.log('what');
    return next(new AppError('Incorrect email or password', 401));
  }

  console.log('hey hey we ok');
  //3) if everything is ok, send token to the client
  createSendToken(user, 200, res);
});

exports.protect = catchAsync(async (req, res, next) => {
  //1) getting token and checking if it exists
  //defining token outside if because if the token was defined inside the if, it would not be available outside because of ES6
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    token = req.headers.authorization.split(' ')[1];
  }
  if (!token) {
    return next(new AppError('You are not logged In', 401));
  }
  //2) verify the token

  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

  //3) check if user still exists

  const currentUser = await User.findById(decoded.id);
  if (!currentUser) {
    return next(new AppError('This user no longer exists', 401));
  }
  //4) check if user changed password after the token was issued

  //   if (currentUser.changedPasswordAfter(decoded.iat)) {
  //     return next(
  //       new AppError(
  //         'You recently changed your password, please log in again to continue',
  //         401
  //       )
  //     );
  //   }

  //grant access to the protected route
  req.user = currentUser;
  console.log('user is: ', req.user);
  next();
});

module.exports.tryLogin = (req, res) => {
  console.log('req user is: ', req.body);
};

module.exports.tryLogout = (req, res) => {
  console.log('tried');
  res.status(202).clearCookies('token').json({
    status: 'success',
  });
};

module.exports.getId = (req, res) => {
  console.log('token is:', req.params.token);
  jwt.verify(req.params.token, process.env.JWT_SECRET, (err, payload) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({
        status: 'success',
        id: payload.id,
      });
    }
  });
};
